package prsistancyLayer.interfaces;

public interface PropertyValue<T extends Comparable<T>> extends Comparable<PropertyValue<T>> {

	public T getValue();
}
