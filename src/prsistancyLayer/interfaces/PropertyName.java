package prsistancyLayer.interfaces;

public interface PropertyName<T extends Comparable<T>> extends Comparable<PropertyName<T>> {

	public T getName();

}
