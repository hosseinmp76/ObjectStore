package prsistancyLayer.interfaces;

import java.util.List;

import javax.swing.tree.TreeModel;

import dB.DBStorageEception;
import prsistancyLayer.implement.ObjectStorageEception;

public interface ObjectStorage {

	public List<SavedObject> getObjects(List<Property> l) throws ObjectStorageEception, DBStorageEception;

	List<SavedObject> getObjectsByID(List<Integer> objectsIDs) throws ObjectStorageEception, DBStorageEception;

	public TreeModel getObjectsTreeModel() throws ObjectStorageEception, DBStorageEception;

	public List<? extends Class<? extends SavedObject>> getObjectTypes() throws ObjectStorageEception;

	public void newObject(List<Property> l) throws ObjectStorageEception, DBStorageEception;

	public List<Integer> searchByFields(String[] split, boolean andOrOr) throws DBStorageEception;
}
