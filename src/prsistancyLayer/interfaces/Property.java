package prsistancyLayer.interfaces;

public interface Property<T, S> extends Comparable<Property<T, S>> {
	public PropertyName getName();

	public PropertyValue getValue();

	public void setValue(PropertyValue value);

}
