package prsistancyLayer.interfaces;

import java.util.List;

public interface SavedObject {

	public int getID();
	public List<Property> getProperties();
}
