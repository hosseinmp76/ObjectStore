package prsistancyLayer.implement;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import prsistancyLayer.abstrac.SavedObjectBase;
import prsistancyLayer.interfaces.Property;

public class SavedObjectStringPropertiesImp extends SavedObjectBase {

	public static String getClassName() {
		return "StringPropObj";
	}

	public static SavedObjectStringPropertiesImp jsonToSavedObjectStringPropertiesImp(int id, String string) {
		StringReader reader = new StringReader(string);
		JsonReader reader2 = Json.createReader(reader);

		JsonObject o = reader2.readObject();
		Set<Entry<String, JsonValue>> t = o.entrySet();
		List<Property> properties = new ArrayList<>();
		for (Entry<String, JsonValue> entry : t) {
			String w = entry.getValue().toString();
			w = w.substring(1, w.length() - 1);
			properties.add(new PropertyBaseImp(new PropertyNameStringImp(entry.getKey()),
					new PropertyValueStringImp(w)));
		}
		return new SavedObjectStringPropertiesImp(id, properties);

	}

	protected SavedObjectStringPropertiesImp(int id, List<Property> properties) {
		super(id, properties);
	}

	@Override
	public String toString() {
		List<Property> l = this.getProperties();
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < l.size(); i++) {
			res.append(l.get(i).toString() + ((i != (l.size() - 1)) ? " - " : ""));
		}
		return res.toString();

	}

}
