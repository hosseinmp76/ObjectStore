package prsistancyLayer.implement;

import prsistancyLayer.interfaces.PropertyName;

public class PropertyNameBase<T extends Comparable<T>> implements PropertyName<T> {

	T name;

	public PropertyNameBase(T name) {
		this.name = name;
	}

	@Override
	public int compareTo(PropertyName<T> o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public T getName() {
		return name;
	}

}
