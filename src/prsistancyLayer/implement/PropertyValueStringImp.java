package prsistancyLayer.implement;

import prsistancyLayer.abstrac.PropertyValueBase;

public class PropertyValueStringImp extends PropertyValueBase<String> {

	public PropertyValueStringImp(String value) {
		super(value);
	}

	@Override
	public String toString() {
		return value;
	}
}
