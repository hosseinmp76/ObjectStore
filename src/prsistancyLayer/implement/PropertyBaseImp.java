package prsistancyLayer.implement;

import prsistancyLayer.abstrac.PropertyBase;
import prsistancyLayer.interfaces.PropertyName;
import prsistancyLayer.interfaces.PropertyValue;

public class PropertyBaseImp<T extends Comparable<T>, S extends Comparable<S>> extends PropertyBase<T, S> {

	public PropertyBaseImp(PropertyName<T> name, PropertyValue<S> value) {
		super(name, value);
	}


}
