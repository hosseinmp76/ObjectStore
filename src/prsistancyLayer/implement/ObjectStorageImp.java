package prsistancyLayer.implement;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreeModel;

import dB.DBBase;
import dB.DBStorageEception;
import prsistancyLayer.interfaces.ObjectStorage;
import prsistancyLayer.interfaces.Property;
import prsistancyLayer.interfaces.SavedObject;

public class ObjectStorageImp implements ObjectStorage {
	private final DBBase db;
	private final Object synchronizationObject = new Object();

	public ObjectStorageImp(DBBase db) {
		this.db = db;
	}

	@Override
	public List<SavedObject> getObjects(List<Property> properties) throws ObjectStorageEception, DBStorageEception {
		if (properties == null) {
			throw new ObjectStorageEception("invalid input in function 'getObjects' in class 'ObjectStorageImp'");
		}
		return this.db.getObjects(properties);
	}

	@Override
	public List<SavedObject> getObjectsByID(List<Integer> objectsIDs) throws ObjectStorageEception, DBStorageEception {
		if (objectsIDs == null) {
			throw new ObjectStorageEception("invalid input in function 'getObjects' in class 'ObjectStorageImp'");
		}
		return this.db.getObjectsByID(objectsIDs);
	}

	@Override
	public TreeModel getObjectsTreeModel() throws DBStorageEception {
		return this.db.getObjectsTreeModel();
	}

	@Override
	public List<Class<? extends SavedObject>> getObjectTypes() throws ObjectStorageEception {
		List<Class<? extends SavedObject>> ans = new ArrayList<>();
		ans.add(SavedObjectStringPropertiesImp.class);
		return ans;
	}

	@Override
	public void newObject(List<Property> properties) throws ObjectStorageEception, DBStorageEception {
		if (properties == null) {
			throw new ObjectStorageEception("invalid input in function 'newObject' in class 'ObjectStorageImp'");
		}
		synchronized (synchronizationObject) {
			this.db.newObject(properties);
		}

	}

	@Override
	public List<Integer> searchByFields(String[] split, boolean andOrOr) throws DBStorageEception {
		return this.db.searchByFields(split, andOrOr);
	}

}
