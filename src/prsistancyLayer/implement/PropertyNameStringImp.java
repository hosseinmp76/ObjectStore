package prsistancyLayer.implement;

public class PropertyNameStringImp extends PropertyNameBase<String> {

	public PropertyNameStringImp(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return name;
	}
}
