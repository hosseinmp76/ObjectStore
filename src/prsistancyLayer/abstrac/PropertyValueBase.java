package prsistancyLayer.abstrac;

import prsistancyLayer.interfaces.PropertyValue;

public abstract class PropertyValueBase<T extends Comparable<T>> implements PropertyValue<T> {

	protected T value;

	public PropertyValueBase(T value) {
		this.value = value;
	}

	@Override
	public int compareTo(PropertyValue<T> o) {
		return this.value.compareTo(o.getValue());
	}

	@Override
	public T getValue() {
		return value;
	}

}
