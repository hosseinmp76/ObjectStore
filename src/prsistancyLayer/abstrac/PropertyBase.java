package prsistancyLayer.abstrac;

import prsistancyLayer.interfaces.Property;
import prsistancyLayer.interfaces.PropertyName;
import prsistancyLayer.interfaces.PropertyValue;

public abstract class PropertyBase<T, S> implements Property<T, S> {

	private PropertyName name;
	private PropertyValue value;

	public PropertyBase(PropertyName name, PropertyValue value) {
		super();
		this.name = name;
		this.value = value;
	}

	@Override
	public int compareTo(Property o) {
		return this.value.compareTo(o.getValue());
	}

	@Override
	public PropertyName getName() {
		return this.name;
	}

	@Override
	public PropertyValue getValue() {
		return value;
	}

	@Override
	public void setValue(PropertyValue value) {
		this.value = value;
	}

}
