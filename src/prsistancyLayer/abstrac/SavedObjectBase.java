package prsistancyLayer.abstrac;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.TableModel;

import prsistancyLayer.interfaces.Property;
import prsistancyLayer.interfaces.SavedObject;

public abstract class SavedObjectBase implements SavedObject {
	private static String[] columnNames(List<SavedObject> so) {
		ArrayList<String> res = new ArrayList<>();
		for (SavedObject savedObject : so) {
			for (Property p : savedObject.getProperties()) {
				res.add(p.getName().toString());
			}
		}
		String[] ans = new String[res.size()];
		int index = 0;
		for (String string : res) {
			ans[index] = string;
			index++;
		}

		ans = myUtills.Algorithm.unique(String.class, ans);
		return ans;
	}

	public static TableModel getTableModel(List<SavedObject> objs) {
		String[] t2 = SavedObjectBase.columnNames(objs);
		String[][] t1 = SavedObjectBase.rowData(t2, objs);

		JTable tem = new JTable(t1, t2);
		return tem.getModel();
	}

	private static String[][] rowData(String[] t2, List<SavedObject> so) {
		String[][] ans = new String[so.size()][t2.length];
		for (int i = 0; i < so.size(); i++) {
			List<Property> ss = so.get(i).getProperties();
			for (Property property : ss) {
				for (int j = 0; j < t2.length; j++) {
					if (t2[j].equals(property.getName().toString())) {
						ans[i][j] = property.getValue().toString();
						break;
					}
				}
			}
		}
		for (int i = 0; i < so.size(); i++) {
			for (int j = 0; j < t2.length; j++) {
				if (ans[i][j] == null) {
					ans[i][j] = "*";
				}
			}
		}
		return ans;
	}

	final int id;

	List<Property> properties;

	protected SavedObjectBase(int id, List<Property> properties) {
		this.id = id;
		this.properties = properties;
	}


	@Override
	public List<Property> getProperties() {
		return this.properties;
	}

}
