package dB.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dB.DBStorageEception;
import uI.AppInstaller;

public class SQLiteImp extends SQLImp {
	public SQLiteImp() {
		super();
	}

	@Override
	public Connection getConnection() {
		String url = AppInstaller.DBUrl;
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}

	@Override
	public List<Integer> searchByFields(String[] fieldsOrValues, boolean andConstrain) throws DBStorageEception {
		try {
			if ((fieldsOrValues == null)) {
				throw new DBStorageEception("Illegal input in getObjectsByID function in SQLImp SQLiteImp");
			}
			String constrainsConstrain = (andConstrain == true ? "AND" : "OR");

			String sql = "SELECT id FROM " + AppInstaller.DBTableName + " WHERE content MATCH (";
			if (fieldsOrValues.length == 0) {
				sql = "SELECT id FROM " + AppInstaller.DBTableName + " WHERE 1";
			} else {
				sql += "'" + fieldsOrValues[0] + "'";
				for (int i = 1; i < fieldsOrValues.length; i++) {
					sql += (" " + constrainsConstrain + " '" + fieldsOrValues[i] + "' ");
				}
				sql += ")";
			}
			Connection conn = this.getConnection();
			Statement stmt = conn.createStatement();
			System.out.println("ERROR " + sql);
			System.out.println(fieldsOrValues.length);
			ResultSet rs = stmt.executeQuery(sql);
			ArrayList<Integer> res = new ArrayList<>();
			while (rs.next()) {
				res.add(rs.getInt("id"));
			}
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBStorageEception("getObjectsByID function in SQLiteImp Class");
		}
	}
}