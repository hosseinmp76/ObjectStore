package dB.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

import dB.DBBase;
import dB.DBStorageEception;
import prsistancyLayer.implement.SavedObjectStringPropertiesImp;
import prsistancyLayer.interfaces.Property;
import prsistancyLayer.interfaces.SavedObject;
import uI.AppInstaller;

public abstract class SQLImp implements DBBase {

	@Override
	public List<SavedObject> getObjects(List<Property> l) throws DBStorageEception {
		throw new RuntimeException("Not implimented");

	}

	@Override
	public List<SavedObject> getObjectsByID(List<Integer> l) throws DBStorageEception {
		try {
			if (l == null) {
				throw new DBStorageEception("Illegal input in getObjectsByID function in SQLImp Class");
			}
			String sql = "SELECT id, content FROM " + AppInstaller.DBTableName + " WHERE id IN (";
			if (l.size() == 0) {
				sql = "SELECT id, content FROM " + AppInstaller.DBTableName + " WHERE 1";
			} else {
				sql += l.get(0);
				for (int i = 1; i < l.size(); i++) {
					sql += (" , " + l.get(i).intValue());
				}
				sql += ")";
			}
			Connection conn = this.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			ArrayList<SavedObject> res = new ArrayList<>();
			while (rs.next()) {
				SavedObject t = SavedObjectStringPropertiesImp.jsonToSavedObjectStringPropertiesImp(rs.getInt("id"),
						rs.getString("content"));
				res.add(t);
			}
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBStorageEception("getObjectsByID function in SQLImp Class");
		}

	}

	@Override
	public TreeModel getObjectsTreeModel() throws DBStorageEception {
		try {

			String sql = "SELECT id, content FROM " + AppInstaller.DBTableName + "";
			Connection conn = this.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");
			while (rs.next()) {
				DefaultMutableTreeNode t = new DefaultMutableTreeNode(
						"" + rs.getInt("id") + ":" + rs.getString("content"));
				root.add(t);
			}
			DefaultTreeModel ans = new DefaultTreeModel(root);
			return ans;
		} catch (SQLException e) {
			throw new DBStorageEception("getObjectsTreeModel");
		}

	}

	@Override
	public void newObject(List<Property> properties) throws DBStorageEception {
		try {
			String sql = "SELECT * FROM " + AppInstaller.DBInfoTableName;
			Connection conn = this.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			JsonObjectBuilder b = Json.createObjectBuilder();
			for (Property p : properties) {
				b.add(p.getName().toString(), p.getValue().toString());
			}
			sql = "INSERT INTO " + AppInstaller.DBTableName + "(id, content) VALUES(?,?)";
			JsonObject o = b.build();

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id + 1);
			pstmt.setString(2, o.toString());
			int x = pstmt.executeUpdate();
			if (x == 1) {
				sql = "UPDATE OSDBInfoTable SET id = " + (id + 1);
				stmt.executeUpdate(sql);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBStorageEception("newObject");
		}
	}

}
