package dB;

import java.sql.Connection;
import java.util.List;

import javax.swing.tree.TreeModel;

import prsistancyLayer.interfaces.Property;
import prsistancyLayer.interfaces.SavedObject;

public interface DBBase {

	Connection getConnection();

	public List<SavedObject> getObjects(List<Property> l) throws DBStorageEception;

	List<SavedObject> getObjectsByID(List<Integer> objectsIDs) throws DBStorageEception;

	public TreeModel getObjectsTreeModel() throws DBStorageEception;

	public void newObject(List<Property> l) throws DBStorageEception;

	List<Integer> searchByFields(String[] split, boolean andOrOr) throws DBStorageEception;
}
