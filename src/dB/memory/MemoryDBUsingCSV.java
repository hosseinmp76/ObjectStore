package dB.memory;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreeModel;

import dB.DBBase;
import dB.DBStorageEception;
import prsistancyLayer.interfaces.Property;
import prsistancyLayer.interfaces.SavedObject;
import uI.CSVImporter;

public class MemoryDBUsingCSV implements DBBase {
	private ArrayList<SavedObject> objects;

	private MemoryDBUsingCSV() {
		this.objects = new ArrayList<>();
	}

	@Override
	public Connection getConnection() {
		// TODO Auto-generated method stub
		return null;
	}

	public MemoryDBUsingCSV getInstanceByImportingCSV() {
		MemoryDBUsingCSV res = new MemoryDBUsingCSV();
		CSVImporter ci = new CSVImporter(res);
		ci.show();
		return res;
	}

	public MemoryDBUsingCSV getInstanceByImportingFile() {
		MemoryDBUsingCSV res = new MemoryDBUsingCSV();
		
		return res;
	}

	@Override
	public List<SavedObject> getObjects(List<Property> l) throws DBStorageEception {
		throw new RuntimeException("Not implimented");
	}

	@Override
	public List<SavedObject> getObjectsByID(List<Integer> objectsIDs) throws DBStorageEception {
		if (objectsIDs == null) {
			throw new DBStorageEception("Illegal input in getObjectsByID function in SQLImp Class");
		}
		if (objectsIDs.size() == 0) {
			return this.objects;
		}
		ArrayList<SavedObject> res = new ArrayList<>();
		for (SavedObject so : this.objects) {
			for (Integer id : objectsIDs) {
				if (so.getID() == id.intValue()) {
					res.add(so);
				}
			}
		}
		return res;
	}

	@Override
	public TreeModel getObjectsTreeModel() throws DBStorageEception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void newObject(List<Property> l) throws DBStorageEception {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Integer> searchByFields(String[] split, boolean andConstrain) {
		// TODO Auto-generated method stub
		return null;
	}

}
