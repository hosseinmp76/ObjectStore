package uI;

import java.awt.HeadlessException;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreeModel;

import dB.DBBase;
import dB.DBStorageEception;
import dB.sql.SQLiteImp;
import prsistancyLayer.implement.ObjectStorageEception;
import prsistancyLayer.interfaces.ObjectStorage;
import prsistancyLayer.interfaces.Property;
import prsistancyLayer.interfaces.SavedObject;

public class App {

	public static List<Class<? extends DBBase>> getDataBases() {
		List<Class<? extends DBBase>> dbs = new ArrayList<>();
		dbs.add(SQLiteImp.class);
		return dbs;
	}

	private final Class<? extends ObjectStorage> storeClass;
	private final Class<? extends DBBase> dBClass;

	public App(Class<? extends ObjectStorage> storeClass, Class<? extends DBBase> dBClass) throws HeadlessException {
		super();
		this.storeClass = storeClass;
		this.dBClass = dBClass;

	}

	public List<SavedObject> getObjectsByID(List<Integer> objectsIDs) throws ObjectStorageEception, DBStorageEception {
		return this.getStore().getObjectsByID(objectsIDs);
	}

	public TreeModel getObjectsTreeModel() throws ObjectStorageEception, DBStorageEception {
		return this.getStore().getObjectsTreeModel();
	}

	public List<? extends Class<? extends SavedObject>> getObjectTypes() throws ObjectStorageEception {
		return this.getStore().getObjectTypes();
	}

	private ObjectStorage getStore() {
		try {
			Constructor<?> ctor = storeClass.getConstructor(DBBase.class);
			Constructor<?> ctor2 = dBClass.getConstructor();
			Object dbb = ctor2.newInstance();
			Object object = ctor.newInstance(new Object[] { dbb });
			return (ObjectStorage) object;
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	void importByCSV(File selectedFile) {
		// TODO Auto-generated method stub

	}

	public void newObject(List<Property> properties) throws ObjectStorageEception, DBStorageEception {
		this.getStore().newObject(properties);
	}




	public void SaveChaneges() {
	}

	public List<Integer> searchByFields(String[] split, boolean andOrOr) throws DBStorageEception {
		return this.getStore().searchByFields(split, andOrOr);
	}

}
