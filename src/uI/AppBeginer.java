package uI;

import java.awt.BorderLayout;
import java.lang.reflect.Constructor;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import dB.DBBase;
import dB.sql.SQLiteImp;
import prsistancyLayer.implement.ObjectStorageImp;
import prsistancyLayer.interfaces.ObjectStorage;

public class AppBeginer extends JFrame {
	private static final Class<? extends ObjectStorage> storeClass = ObjectStorageImp.class;

	private static final Class<? extends DBBase> defaultDBClass = SQLiteImp.class;
	private static Class<? extends DBBase> dBClass;

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		AppBeginer ab = new AppBeginer();
		ab.show();

	}

	public AppBeginer() {

		JPanel panel = new JPanel();
		this.setSize(500, 250);
		this.setLocation(450, 75);

		panel.setLayout(new BorderLayout(0, 0));

		JComboBox comboBox = new JComboBox();
		panel.add(comboBox, BorderLayout.CENTER);

		JButton btnStart = new JButton("Start");
		List<Class<? extends DBBase>> t = App.getDataBases();
		for (Class<? extends DBBase> element : t) {
			comboBox.addItem(element.getName());
		}
		btnStart.addActionListener(e -> SwingUtilities.invokeLater(() -> {
			Class<? extends DBBase> c;
			try {
				c = (Class<? extends DBBase>) Class.forName((String) comboBox.getSelectedItem());
				Constructor<?> ctor2 = c.getConstructor();
				Object dbb = ctor2.newInstance();

			} catch (Exception ee) {
				c = defaultDBClass;
				ee.printStackTrace();
			}
			JavaUI.launch(new App(storeClass, c));
			dispose();
		}));
		panel.add(btnStart, BorderLayout.SOUTH);

		getContentPane().add(panel, BorderLayout.CENTER);

		JLabel lblDatabase = new JLabel("Database");
		lblDatabase.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblDatabase, BorderLayout.NORTH);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

}
