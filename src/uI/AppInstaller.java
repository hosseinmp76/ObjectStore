package uI;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;

public class AppInstaller extends JFrame {
	public static final String DBUrl = "jdbc:sqlite:D:/sqlite/db/ObjectStoreDB.db";
	public static final String DBTableName = "OSDBTable";
	public static final String DBInfoTableName = "OSDBInfoTable";

	public static void createNewDatabase() throws SQLException {

		String url = DBUrl;

		Connection conn = DriverManager.getConnection(url);
		if (conn == null) {
			throw new RuntimeException();
		}
		DatabaseMetaData meta = conn.getMetaData();
		String sql = "CREATE TABLE IF NOT EXISTS " + DBInfoTableName + " ( id integer );";
		Statement stmt = conn.createStatement();
		stmt.execute(sql);
		sql = "CREATE VIRTUAL TABLE IF NOT EXISTS " + DBTableName + " USING fts5(id , content ) ";
		stmt = conn.createStatement();
		stmt.execute(sql);
	}

	public static void main(String[] args) {
		try {
			createNewDatabase();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
