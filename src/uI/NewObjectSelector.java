package uI;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import prsistancyLayer.implement.PropertyBaseImp;
import prsistancyLayer.implement.PropertyNameStringImp;
import prsistancyLayer.implement.PropertyValueStringImp;
import prsistancyLayer.interfaces.Property;

public class NewObjectSelector extends JFrame {
	private List<Property> properties;

	private App baseapp;
	private JavaUI ui;

	public NewObjectSelector(App baseApp, JavaUI ui) {
		super();
		this.baseapp = baseApp;
		this.ui = ui;
		this.ini();
	}

	public void ini() {
		properties = new ArrayList<Property>();
		this.setSize(452, 300);
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JButton btnAdd = new JButton("add");
		btnAdd.addActionListener(e -> {
			try {
				this.baseapp.newObject(properties);
				this.ui.reValidateTree();
				this.dispose();
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		});
		panel.add(btnAdd, BorderLayout.SOUTH);

		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);

		JTextPane textPane = new JTextPane();
		textPane.setBounds(33, 73, 136, 72);
		panel_1.add(textPane);

		JTextPane textPane_1 = new JTextPane();
		textPane_1.setBounds(179, 73, 98, 72);
		panel_1.add(textPane_1);

		JLabel lblPropertyName = new JLabel("property name");
		lblPropertyName.setBounds(35, 11, 88, 39);
		panel_1.add(lblPropertyName);

		JLabel lblPropertyValue = new JLabel("property value");
		lblPropertyValue.setBounds(174, 23, 103, 39);
		panel_1.add(lblPropertyValue);
		JButton btnAddProperty = new JButton("add Property");
		btnAddProperty.addActionListener(e -> {
			properties.add(new PropertyBaseImp(new PropertyNameStringImp(textPane.getText()),
					new PropertyValueStringImp(textPane_1.getText())));
			textPane.setText("");
			textPane_1.setText("");
		});
		btnAddProperty.setBounds(281, 106, 106, 39);
		panel_1.add(btnAddProperty);

	}
}
