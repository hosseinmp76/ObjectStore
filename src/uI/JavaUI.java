package uI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import dB.DBStorageEception;
import prsistancyLayer.abstrac.SavedObjectBase;
import prsistancyLayer.implement.ObjectStorageEception;
import prsistancyLayer.interfaces.SavedObject;

public class JavaUI extends JFrame {
	public static void launch(App baseApp) {
		JavaUI a = new JavaUI(baseApp);
		a.reValidateTree();
		a.setDefaultCloseOperation(EXIT_ON_CLOSE);
		a.setSize(900, 500);
		a.setLocation(300, 50);
		a.show();
	}

	private JTree tree;
	private final App baseApp;
	private JTable table;
	boolean changedSearcheText;

	public JavaUI(App baseApp) {
		super();
		this.baseApp = baseApp;
		try {
			ini();
		} catch (ObjectStorageEception e) {
			e.printStackTrace();
		}
	}

	public void ini() throws ObjectStorageEception {

		JMenuBar menuBar = new JMenuBar();
		getContentPane().add(menuBar, BorderLayout.NORTH);

		JMenu mnFile = new JMenu("file");
		menuBar.add(mnFile);

		JMenuItem mntmImportCsv = new JMenuItem("import csv");
		mntmImportCsv.addActionListener(e -> {

			CSVImporter tt = new CSVImporter();
			tt.setSize(250, 250);
			tt.setLocation(250, 250);
			tt.show();

			FileNameExtensionFilter filter = new FileNameExtensionFilter("speardsheet: csv", "csv");
			tt.setFileFilter(filter);
			int result = tt.showOpenDialog(this);
			File selectedFile = tt.getSelectedFile();
			this.baseApp.importByCSV(selectedFile);
		});

		mnFile.add(mntmImportCsv);

		JMenu mnVisualize = new JMenu("visualize");
		menuBar.add(mnVisualize);

		JMenuItem mntmChart = new JMenuItem("chart");
		mnVisualize.add(mntmChart);
		JSplitPane mainSplitPane = new JSplitPane();
		getContentPane().add(mainSplitPane, BorderLayout.CENTER);

		JSplitPane sideSplitPane = new JSplitPane();
		sideSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		sideSplitPane.setBounds(6, 106, 182, 88);

		JSplitPane newAndSearchSplitPane = new JSplitPane();
		newAndSearchSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

		JPanel newPartPanel = new JPanel();
		newPartPanel.setBounds(0, 0, 249, 259);
		JPanel searchPanel = new JPanel();
		searchPanel.setBounds(0, 0, 275, 259);

		newAndSearchSplitPane.setLeftComponent(newPartPanel);
		newAndSearchSplitPane.setRightComponent(searchPanel);
		searchPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JScrollPane searchText = new JScrollPane();
		searchPanel.add(searchText);
		searchPanel.setBounds(0, 0, 275, 240);
		changedSearcheText = false;
		JTextArea txtrAttrToSearch_1 = new JTextArea();
		txtrAttrToSearch_1.setText("attr to search");
		searchText.setViewportView(txtrAttrToSearch_1);
		txtrAttrToSearch_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (changedSearcheText == false) {
					txtrAttrToSearch_1.setText("");
					changedSearcheText = true;
				}

			}
		});
		JCheckBox chckbxAndOr = new JCheckBox("And | Or");
		searchPanel.add(chckbxAndOr);

		JButton btnNewButton_3 = new JButton("New button");
		btnNewButton_3.addActionListener(e -> {
			List<Integer> ObjectsIDs = null;
			try {
				if (changedSearcheText == true) {
					String[] constrains = txtrAttrToSearch_1.getText().split("\n");
					if (txtrAttrToSearch_1.getText().length() == 0) {
						constrains = new String[0];
					}
					ObjectsIDs = baseApp.searchByFields(constrains, chckbxAndOr.isSelected());
				} else {
					JOptionPane.showMessageDialog(null, "enter sth to search or delete text to see all objects");
					return;
				}
			} catch (DBStorageEception e1) {
				e1.printStackTrace();
			}
			reValidateViewPart(ObjectsIDs);
		});

		searchPanel.add(btnNewButton_3);

		JTextArea txtrAttrToSearch = new JTextArea();
		txtrAttrToSearch.setText("Attr to search");

		sideSplitPane.setLeftComponent(newAndSearchSplitPane);

		mainSplitPane.setLeftComponent(sideSplitPane);

		JButton btnNewobj = new JButton("newObj");
		btnNewobj.addActionListener(e -> {
			new NewObjectSelector(this.baseApp, this).show();
		});
		newPartPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblNewLabel = new JLabel("Object Type");
		newPartPanel.add(lblNewLabel);
		JPanel panel_2 = new JPanel();
		newPartPanel.add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JComboBox comboBox = new JComboBox();

		List<? extends Class<? extends SavedObject>> t = this.baseApp.getObjectTypes();
		for (Class<? extends SavedObject> element : t) {
			Method m;
			try {
				m = element.getMethod("getClassName", null);
				comboBox.addItem(m.invoke(null, null));
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e1) {
				e1.printStackTrace();
			}

		}
		panel_2.add(comboBox);
		panel_2.add(btnNewobj);

		JPanel panel_3 = new JPanel();
		sideSplitPane.setRightComponent(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));

		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");

		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		tree = new JTree(root);
		panel_1.add(tree);

		JButton btnNewButton = new JButton("view");
		btnNewButton.addActionListener(e -> {
			TreeSelectionModel rr = tree.getSelectionModel();
			TreePath[] selections = rr.getSelectionPaths();
			ArrayList<Integer> al = new ArrayList<>();
			for (TreePath c : selections) {
				DefaultMutableTreeNode x = (DefaultMutableTreeNode) c.getLastPathComponent();
				String pattern = "^([0-9]+)";
				System.out.println(x.toString());
				Pattern r = Pattern.compile(pattern);
				Matcher m = r.matcher(x.toString());
				m.find();
				al.add(Integer.parseInt(m.group(0)));
				reValidateViewPart(al);
			}

		});
		panel_3.add(btnNewButton, BorderLayout.SOUTH);

		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BorderLayout(0, 0));
		JScrollPane scrollPane = new JScrollPane();
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setShowHorizontalLines(true);
		table.setShowVerticalLines(true);
		rightPanel.add(scrollPane, BorderLayout.CENTER);
		mainSplitPane.setRightComponent(rightPanel);

		JButton btnNewButton_1 = new JButton("save changes");
		btnNewButton_1.addActionListener(e -> SaveChaneges());
		rightPanel.add(btnNewButton_1, BorderLayout.SOUTH);

	}

	public void reValidateTree() {
		DefaultTreeModel tm = (DefaultTreeModel) this.tree.getModel();
		TreeModel otm;

		try {
			otm = this.baseApp.getObjectsTreeModel();
			Object o = otm.getRoot();
			tm.setRoot((TreeNode) o);
		} catch (ObjectStorageEception | DBStorageEception e) {
			e.printStackTrace();
		}

		tm.reload();

	}

	public void reValidateViewPart(List<Integer> ObjectsIDs) {
		List<SavedObject> objs;
		TableModel tt;

		try {
			objs = this.baseApp.getObjectsByID(ObjectsIDs);
			tt = SavedObjectBase.getTableModel(objs);
			this.table.setModel(tt);
		} catch (ObjectStorageEception | DBStorageEception e) {
			e.printStackTrace();
		}

		this.table.revalidate();
	}

	public void SaveChaneges() {
	}

}
